<?php
    //define('BASE_URL', 'http://localhost/icms/');
    function confirm_query($result, $query) {
        global $dbc;
        if(!$result) {
            die("Query {$query} \n<br/> MySQL Error: " .mysqli_error($dbc));
        }
    }
    
    function redirect_to($page = 'index.php') {
        define('BASE_URL', 'http://localhost/icms/');
        $url = BASE_URL . $page;
        header("Location: $url");
        exit();
    }
    function the_excerpt($text) {
        $sanitized = htmlentities($text, ENT_COMPAT, 'UTF-8');
        if(strlen($sanitized) > 400) {
            $cutString = substr($text,0,400);
            $words = substr($text, 0, strrpos($cutString, ' '));
            return $words;
        } else {
            return $text;
        }
    } // End the_excerpt
    function validate_id($id) {
        if(isset($id) && filter_var($id, FILTER_VALIDATE_INT, array('min_range' => 1))) {
                $val_id = $id;
                return $val_id;
            } else {
                return NULL;
            }
    }//end validate id

    function get_page_by_id($id) {
        global $dbc;
        $q = "SELECT p.page_name, p.page_id, p.content,"; 
        $q .= " DATE_FORMAT(p.post_on, '%b %d, %y') AS date, ";
        $q .= " CONCAT_WS(' ', u.first_name, u.last_name) AS name, u.user_id ";
        $q .= " FROM pages AS p ";
        $q .= " INNER JOIN users AS u ";
        $q .= " USING (user_id) ";
        $q .= " WHERE p.page_id={$id}";
        $q .= " ORDER BY date ASC LIMIT 1";
        $result = mysqli_query($dbc,$q);
        confirm_query($result, $q);
        return $result;
    } //end get_page_by_id

    //tap paragraph tu CSDL
    function the_content($text) {
        $sanitized = htmlentities($text, ENT_COMPAT, 'UTF-8');
      return str_replace(array("\r\n", "\n"), array("<p>", "</p>"), $sanitized);
    }
    function captcha() {
        $qna = array(
            1 => array('question' => 'once plus once', 'answer' => 2),
            2 => array('question' => 'once plus two', 'answer' => 3),
            3 => array('question' => 'once plus three', 'answer' => 4),
            4 => array('question' => 'once plus four', 'answer' => 5)
        );
        $rand_key = array_rand($qna);
        $_SESSION['q'] = $qna[$rand_key];
        return $question = $qna[$rand_key]['question'];
    }//end function captcha
//Phantrang
    function pagination($aid, $display = 2){
        global $dbc;
        global $start;
        if(isset($_GET['p']) && filter_var($_GET['p'], FILTER_VALIDATE_INT, array('min_range' => 1))) {
            $page = $_GET['p'];
        } else {
            // Nếu biến p không có, sẽ truy vấn CSDL để tìm xem có bao nhiêu page để hiển thị
            $q = "SELECT COUNT(page_id) FROM pages";
            $r = mysqli_query($dbc, $q);
            confirm_query($r, $q);
            list($record) = mysqli_fetch_array($r, MYSQLI_NUM);
            
            // Tìm số trang bằng cách chia số dữ liệu cho số display
            if($record > $display) {
                $page = ceil($record/$display);
            } else {
                $page = 1;
            }
        }
        $output = "<ul class='pagination'>";
        if($page > 1) {
            $current_page = ($start/$display) + 1;
            // Nếu không phải ở trang đầu (hoặc 1) thì sẽ hiển thị Trang trước.
            if($current_page != 1) {
                $output .= "<li><a href='author.php?aid={$aid}&s=".($start - $display)."&p={$page}'>Previous</a></li>";
            }
            // Hiển thị những phần số còn lại của trang
            for($i = 1; $i <= $page; $i++) {
                if($i != $current_page) {
                    $output .= "<li><a href='author.php?aid={$aid}&s=".($display * ($i - 1))."&p={$page}'>{$i}</a></li>";
                } else {
                    $output .= "<li class='current'>{$i}</li>";
                }
            }// END FOR LOOP
            // Nếu không phải trang cuối, thì hiển thị trang kế.
            if($current_page != $page) {
                $page = $page + 1;
                $output .= "<li><a href='author.php?aid={$aid}&s=".($start + $display)."&p={$page}'>Next</a></li>";
            }
        } // END pagination section
            $output .= "</ul>";
            return $output;
    } // END pagination
    //Kiem tra nguoi dung dang nhap chua?
    function is_logged_in() {
        if(!isset($_SESSION['uid'])) {
            redirect_to('login.php');
        }
    }
    //Thong bao loi
    function report_error($mgs) {
        if(!empty($mgs)) {
            foreach ($mgs as $m) {
                echo $m;
            }
        }
    }

    //Kiem tra co phai la admin hay khong
    function is_admin() {
        return isset($_SESSION['user_level']) && ($_SESSION['user_level'] == 2);
    }
    //Kiem tra nguoi dung co the vao trang admin hay khong.
    function admin_access() {
        if(!is_admin()) {
            redirect_to();
        }
    }
    //Dem so view trang
    function view_counter($pg_id) {
        $ip = $_SERVER['REMOTE_ADDR'];
        global $dbc;
        // Truy van CSDL de xem page view
        $q = "SELECT num_view, user_ip FROM page_views WHERE page_id = {$pg_id}";
        $r = mysqli_query($dbc, $q); confirm_query($r, $q);
        if(mysqli_num_rows($r) > 0) {
            
            // Neu ket qua tra ve, co nghia la da ton tai trong table, Update page view
            list($num_view, $db_ip) = mysqli_fetch_array($r, MYSQLI_NUM);
            
            // So sanh IP trong CSDL va IP cua nguoi dung, neu khac nhau thi se update CSDL
            if($db_ip !== $ip) {
            $q = "UPDATE page_views SET num_view = (num_view + 1) WHERE page_id = {$pg_id} LIMIT 1";
            $r = mysqli_query($dbc, $q); confirm_query($r, $q);
            }
        } else {
            // Neu ko co ket qua tra ve, thi se insert vao table.
            $q = "INSERT INTO page_views (page_id, num_view, user_ip) VALUES ({$pg_id}, 1, '{$ip}')";
            $r = mysqli_query($dbc, $q); confirm_query($r, $q);
            $num_view = 1;
        }
        return $num_view;
    }// ENd view_counter
    //Ham truy xuat du lieu nguoi dung
    function fetch_user($user_id) {
        global $dbc;
        $q = "SELECT * FROM users WHERE user_id = {$user_id}";
        $r = mysqli_query($dbc, $q);
        confirm_query($r, $q);

        if(mysqli_num_rows($r) > 0) {
            return $result_set = mysqli_fetch_array($r, MYSQLI_ASSOC);
        } else {
            return FALSE;
            }
    }//end fetch user
    function fetch_users($order) {
        global $dbc;

        $q = "SELECT * FROM users ORDER BY {$order} ASC";
        $r = mysqli_query($dbc, $q);
        confirm_query($r, $q);

        if(mysqli_num_rows($r) > 1) {
            //Tao array de luu ket qua
            $users = array();
            //Neu co gia tri tra ve
            while ($results = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
                $users[] = $results;
            } //end while 
            return $users;  
        } else {
            return FALSE;
        }
    } //end fetch_users

    // Ham de sap xep thu tu cua bang USERS
    function sort_table_users($order) {
        switch ($order) {
            case 'fn':
            $order_by = "first_name";
            break;
            
            case 'ln':
            $order_by = "last_name";
            break;
            
            case 'e':
            $order_by = "email";
            break;
            
            case 'ul':
            $order_by = "user_level";
            break;
            
            default:
            $order_by = "first_name";
            break;
        }
        return $order_by;
    } // END sort_table_users
    function check_db_conn() {
        if(mysqli_connect_error()) {
            echo "Connect fail: ".mysqli_connect_error();
            exit();
        }
    }
    function current_page($page) {
        if(basename($_SERVER['SCRIPT_NAME']) == $page) {
            echo "class= 'here'";
        }
    }
?>