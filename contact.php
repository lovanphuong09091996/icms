<?php $title = 'Contact US'; include('includes/header.php');?>
<?php include('includes/mysqli_connect.php');?>
<?php include('includes/function.php');?>
<?php include('includes/sidebar-a.php'); ?>
<div id="content">
<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST') {  
        //tao bien de bao loi
        $errors = array();
        if(empty($_POST['name'])) {
            $errors[] = 'name';
        }
        //kiem tra tinh hop le cua email dung bieu thuc chinh quy (.)' '
        //[a-zA-Z0-9_] = \w
        if(!preg_match('/^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$/', $_POST['email'])) {
            $errors = 'email';
        }
        //kiem tra noi dung tin nhan
        if(empty($_POST['comment'])) {
            $errors ='comment';
        }
        //kiem tra cos loi form hay khong, neu co loi gui email
        if(empty($errors)) {
            $body = "Name: {$_POST['name']}\n\n Comment: \n ". strip_tags($_POST['comment']);
            $body = wordwrap($body, 70);
            if(mail('lovanphuong0909@gmail.com','Contact form submission',$body, 'From: localhost@gmail.com\r')) {
                echo "<p class ='success'> Thanks you contacting me.</p>";
            } else {
                echo "<p class ='warning'> Sorry, your email could not be sent.</p>";
            } 
        } else {
            echo "<p class ='warning'> Please fill out all the required fields.</p>";
        }
    }
?>
<form id="contact" action="" method="post">
    <fieldset>
    	<legend>Contact</legend>
            <div>
                <label for="Name">Your Name: <span class="required">*</span>
                    <?php 
                        if(isset($errors) && in_array('name',$errors)) {
                            echo "<span class='warning'>Please enter your name.</span>";
                            }
                    ?>
                </label>
                <input type="text" name="name" id="name" value="<?php if(isset($_POST['name'])) {echo htmlentities($_POST['name'], ENT_COMPAT, 'UTF-8');} ?>" size="20" maxlength="80" tabindex="1" />
            </div>
        	<div>
                <label for="email">Email: <span class="required">*</span>
                <?php 
                        if(isset($errors) && in_array('email',$errors)) {
                            echo "<span class='warning'>Please enter your email.</span>";
                            }
                    ?>
                </label>
                <input type="text" name="email" id="email" value="<?php if(isset($_POST['email'])) {echo htmlentities($_POST['email'], ENT_COMPAT, 'UTF-8');} ?>" size="20" maxlength="80" tabindex="2" />
            </div>
            <div>
                <label for="comment">Your Message: <span class="required">*</span>
                    <?php   
                        if(isset($errors) && in_array('comment',$errors)) {
                            echo "<span class='warning'>Please enter your message.</span>";
                            }
                    ?>
                </label>
                <div id="comment"><textarea name="comment" rows="10" cols="45" tabindex="3"><?php if(isset($_POST['comment'])) {echo htmlentities($_POST['comment'], ENT_COMPAT, 'UTF-8');} ?></textarea></div>
            </div>
    </fieldset>
    <div><input type="submit" name="submit" value="Send Email" /></div>
</form>
</div><!--end content-->
<?php include('includes/sidebar-b.php');?>
<?php include('includes/footer.php'); ?>