<?php  
    include('includes/function.php'); 
    include('includes/mysqli_connect.php');

    if($pid = validate_id($_GET['pid'])) {
        $pid = $_GET['pid'];
        //neu pid hop le thi truy van csdl
        $set = get_page_by_id($pid);
        $page_view = view_counter($pid);
        $posts = array();
            if(mysqli_num_rows($set) > 0) {
                //neu co POST hien thi ra man hinh
                $pages = mysqli_fetch_array($set, MYSQLI_ASSOC);
                $title = $pages['page_name'];
                $posts[] = array(
                    'page_name' => $pages['page_name'], 
                    'content'   => $pages['content'], 
                    'author'    => $pages['name'], 
                    'post_on'   => $pages['date'],
                    'aid'       => $pages['user_id']
                );
            } else {
                echo "<p>There are currenlty no post in this category</p>";
            }
    } else {
        //neu pid khong hop le thi dieu huong ve index.php.
        redirect_to('');
    }
    include('includes/header.php');
    include('includes/sidebar-a.php');
?>
<div id="content">
    <?php
    foreach($posts as $post) {
        echo "
        <div class='post'>
            <h2>{$post['page_name']}</h2>
            <p>".the_content($post['content'])."</p>
            <p class='meta'><strong>Posted by: 
                </strong><a href='author.php?aid={$post['aid']}'>{$post['author']}</a> |
                <strong> On: </strong>{$post['post_on']}
                <strong> Page Views: </strong>{$page_view}
            </p>
        </div> 
        ";
    } //end foreach
     ?>
     <?php include('includes/comment_form.php'); ?>
</div>
<?php 
    include('includes/sidebar-b.php');
    include('includes/footer.php'); 
?>
<!--end content-->