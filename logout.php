<?php $title = 'Logout'; include('includes/header.php');?>
<?php include('includes/mysqli_connect.php');?>
<?php include('includes/function.php');?>
<?php include('includes/sidebar-a.php'); ?>
<div id="content">
    <?php 
        if(!isset($_SESSION['last_name'])) {
            redirect_to();
        } else {
            $_SESSION = array(); //Xoa het array cua session
            session_destroy(); //Destroy sessesion
            setcookie(session_name(), '', time()-3600);
        }
        echo "<h2>You are now loged out.</h2>";
    ?>
</div><!--end content-->
<?php include('includes/sidebar-b.php');?>
<?php include('includes/footer.php'); ?>