/*
 Navicat Premium Data Transfer

 Source Server         : testConnection
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : izcms

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 07/02/2020 16:03:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `cat_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `position` tinyint(5) NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (11, 1, 'History', 1);
INSERT INTO `categories` VALUES (12, 1, 'HTML', 2);
INSERT INTO `categories` VALUES (13, 1, 'About', 3);
INSERT INTO `categories` VALUES (14, 1, 'Contact US', 4);
INSERT INTO `categories` VALUES (15, 1, 'Forum', 4);

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NULL DEFAULT NULL,
  `author` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `comment_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES (1, 3, 'lo van phuong', 'lovanphuong09091996@gmail.com', 'Good is page!', '2020-02-07 10:15:51');
INSERT INTO `comments` VALUES (2, 3, 'Bot', 'doilahuao@gmail.com', 'Page is izCMS!', '2020-02-07 10:50:08');
INSERT INTO `comments` VALUES (3, 3, 'Bot', 'doilahuao@gmail.com', 'Page is izCMS!', '2020-02-07 11:50:17');
INSERT INTO `comments` VALUES (4, 3, 'Bot', 'doilahuao@gmail.com', 'Page is izCMS!', '2020-02-07 11:50:47');
INSERT INTO `comments` VALUES (5, 3, 'Bot', 'doilahuao@gmail.com', 'Page is izCMS!', '2020-02-07 11:51:16');
INSERT INTO `comments` VALUES (6, 3, 'Bot', 'doilahuao@gmail.com', 'Page is izCMS!', '2020-02-07 11:53:21');
INSERT INTO `comments` VALUES (7, 3, 'Phuong', 'lophuong@gmail.com', 'This is a great post.', '2020-02-07 12:01:07');
INSERT INTO `comments` VALUES (8, 3, 'Phuong', 'lophuong@gmail.com', 'This is a great post.', '2020-02-07 12:04:58');
INSERT INTO `comments` VALUES (9, 3, 'LO PHUONG', 'lophuong@gmail.com', 'page is busy!', '2020-02-07 12:08:22');
INSERT INTO `comments` VALUES (10, 3, 'LO PHUONG', 'lophuong@gmail.com', 'page is busy!', '2020-02-07 12:10:41');
INSERT INTO `comments` VALUES (11, 3, 'LO PHUONG', 'lophuong@gmail.com', 'page is busy!', '2020-02-07 13:10:23');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `cat_id` int(11) NULL DEFAULT NULL,
  `page_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `position` tinyint(5) NULL DEFAULT NULL,
  `post_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`page_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (3, 1, 12, 'Borona', 'Mặc trên mình bộ đồ bảo hộ màu xanh, đeo kính và khẩu trang y tế, ông Zhang Lei – một tài xế taxi làm việc tại Vũ Hán, vẫn hàng ngày chở các cư dân tại vùng tâm dịch, đặc biệt là những người già, đi mua thực phẩm, thuốc men và tới các bệnh viện.\r\n\r\nTheo ông Zhang chia sẻ, một ngày của ông thường rất dài. Ông phải làm việc 12 tiếng/ngày và đi khắp nơi trong thành phố. Những con đường tại Vũ Hán giờ đây đều rất vắng vẻ vì lệnh phong tỏa, phương tiện công cộng không hoạt động nhằm hạn chế sự lây lan của virus Corona.\r\n\r\nLái chiếc ô tô màu xanh và trắng, ông Zhang là một trong số ít người hàng ngày vẫn di chuyển trên các con đường của Vũ Hán. Ông là một trong những tài xế chạy xe tình nguyện, không lấy tiền công nhằm giảm bớt sự khó khăn về giao thông tại Vũ Hán lúc này.\r\n\r\nÔng Zang phải mặc đầy đủ đồ bảo hộ khi lái xe và không được phép chở người có dấu hiệu nhiễm virus Corona, đó là công việc của xe cứu thương. Hầu hết “khách hàng” của ông là những người nghèo và người cao tuổi.', 2, '2020-02-06 16:13:44');
INSERT INTO `pages` VALUES (4, 1, 13, 'name3', 'Mặc trên mình bộ đồ bảo hộ màu xanh, đeo kính và khẩu trang y tế, ông Zhang Lei – một tài xế taxi làm việc tại Vũ Hán, vẫn hàng ngày chở các cư dân tại vùng tâm dịch, đặc biệt là những người già, đi mua thực phẩm, thuốc men và tới các bệnh viện. Theo ông Zhang chia sẻ, một ngày của ông thường rất dài. Ông phải làm việc 12 tiếng/ngày và đi khắp nơi trong thành phố. Những con đường tại Vũ Hán giờ đây đều rất vắng vẻ vì lệnh phong tỏa, phương tiện công cộng không hoạt động nhằm hạn chế sự lây lan của virus Corona. Lái chiếc ô tô màu xanh và trắng, ông Zhang là một trong số ít người hàng ngày vẫn di chuyển trên các con đường của Vũ Hán. Ông là một trong những tài xế chạy xe tình nguyện, không lấy tiền công nhằm giảm bớt sự khó khăn về giao thông tại Vũ Hán lúc này. Ông Zang phải mặc đầy đủ đồ bảo hộ khi lái xe và không được phép chở người có dấu hiệu nhiễm virus Corona, đó là công việc của xe cứu thương. Hầu hết “khách hàng” của ông là những người nghèo và người cao tuổi.', 4, '2020-02-06 16:06:53');
INSERT INTO `pages` VALUES (5, 1, 14, 'name4', 'Mặc trên mình bộ đồ bảo hộ màu xanh, đeo kính và khẩu trang y tế, ông Zhang Lei – một tài xế taxi làm việc tại Vũ Hán, vẫn hàng ngày chở các cư dân tại vùng tâm dịch, đặc biệt là những người già, đi mua thực phẩm, thuốc men và tới các bệnh viện. Theo ông Zhang chia sẻ, một ngày của ông thường rất dài. Ông phải làm việc 12 tiếng/ngày và đi khắp nơi trong thành phố. Những con đường tại Vũ Hán giờ đây đều rất vắng vẻ vì lệnh phong tỏa, phương tiện công cộng không hoạt động nhằm hạn chế sự lây lan của virus Corona. Lái chiếc ô tô màu xanh và trắng, ông Zhang là một trong số ít người hàng ngày vẫn di chuyển trên các con đường của Vũ Hán. Ông là một trong những tài xế chạy xe tình nguyện, không lấy tiền công nhằm giảm bớt sự khó khăn về giao thông tại Vũ Hán lúc này. Ông Zang phải mặc đầy đủ đồ bảo hộ khi lái xe và không được phép chở người có dấu hiệu nhiễm virus Corona, đó là công việc của xe cứu thương. Hầu hết “khách hàng” của ông là những người nghèo và người cao tuổi.', 5, '2020-02-06 16:07:26');
INSERT INTO `pages` VALUES (6, 1, 15, 'name5', 'Mặc trên mình bộ đồ bảo hộ màu xanh, đeo kính và khẩu trang y tế, ông Zhang Lei – một tài xế taxi làm việc tại Vũ Hán, vẫn hàng ngày chở các cư dân tại vùng tâm dịch, đặc biệt là những người già, đi mua thực phẩm, thuốc men và tới các bệnh viện. Theo ông Zhang chia sẻ, một ngày của ông thường rất dài. Ông phải làm việc 12 tiếng/ngày và đi khắp nơi trong thành phố. Những con đường tại Vũ Hán giờ đây đều rất vắng vẻ vì lệnh phong tỏa, phương tiện công cộng không hoạt động nhằm hạn chế sự lây lan của virus Corona. Lái chiếc ô tô màu xanh và trắng, ông Zhang là một trong số ít người hàng ngày vẫn di chuyển trên các con đường của Vũ Hán. Ông là một trong những tài xế chạy xe tình nguyện, không lấy tiền công nhằm giảm bớt sự khó khăn về giao thông tại Vũ Hán lúc này. Ông Zang phải mặc đầy đủ đồ bảo hộ khi lái xe và không được phép chở người có dấu hiệu nhiễm virus Corona, đó là công việc của xe cứu thương. Hầu hết “khách hàng” của ông là những người nghèo và người cao tuổi.', 6, '2020-02-06 16:07:17');
INSERT INTO `pages` VALUES (7, 1, 14, 'name6', 'Mặc trên mình bộ đồ bảo hộ màu xanh, đeo kính và khẩu trang y tế, ông Zhang Lei – một tài xế taxi làm việc tại Vũ Hán, vẫn hàng ngày chở các cư dân tại vùng tâm dịch, đặc biệt là những người già, đi mua thực phẩm, thuốc men và tới các bệnh viện. Theo ông Zhang chia sẻ, một ngày của ông thường rất dài. Ông phải làm việc 12 tiếng/ngày và đi khắp nơi trong thành phố. Những con đường tại Vũ Hán giờ đây đều rất vắng vẻ vì lệnh phong tỏa, phương tiện công cộng không hoạt động nhằm hạn chế sự lây lan của virus Corona. Lái chiếc ô tô màu xanh và trắng, ông Zhang là một trong số ít người hàng ngày vẫn di chuyển trên các con đường của Vũ Hán. Ông là một trong những tài xế chạy xe tình nguyện, không lấy tiền công nhằm giảm bớt sự khó khăn về giao thông tại Vũ Hán lúc này. Ông Zang phải mặc đầy đủ đồ bảo hộ khi lái xe và không được phép chở người có dấu hiệu nhiễm virus Corona, đó là công việc của xe cứu thương. Hầu hết “khách hàng” của ông là những người nghèo và người cao tuổi.', 4, '2020-02-06 16:07:08');
INSERT INTO `pages` VALUES (8, 1, 14, 'name1', 'Nhiều người dùng mạng xã hội đã chia sẻ và dành lời khen ngợi cho công việc của ông Zhang và một số tài xế tình nguyện tại vùng dịch như ông.\r\n\r\n“Thứ nhất là vì buồn chán. Thứ hai là để phục vụ mọi người. Tôi nghĩ tôi nên làm gì đó để đóng góp cho xã hội vào những lúc như thế này”, ông Zhang cho biết.\r\n\r\nCông việc của ông Zhang rất vất vả, vì chỉ có khoảng 4 tài xế tình nguyện như ông phục vụ tại mỗi khu dân cư trong thành phố.\r\n\r\n“Tất nhiên là chúng tôi rất lo lắng về việc có thể bị lây bệnh. Gia đình tôi cũng rất lo, họ không muốn chúng tôi rời nhà”, ông Zhang tâm sự về công việc của mình.\r\n\r\nÔng Zhang cũng cho biết rằng, ngôi làng mà ông đang sống đến nay vẫn chưa ghi nhận một ca nhiễm virus Corona nào.\r\n\r\n “Một ai đó như tôi là điều nguy hiểm đối với họ. Tôi từng nhận được một thông báo từ làng của mình, yêu cầu chấm dứt ngay công việc này vì tôi làm việc quá gần chợ hải sản Huanan”, ông Zhang cho biết.', 5, '2020-02-06 15:56:27');
INSERT INTO `pages` VALUES (9, 1, 12, 'Phòng chống virut Corona', 'Theo Nhân dân Nhật báo, đảng và chính phủ Trung Quốc đã chỉ đạo và huy động mọi nguồn lực trong cuộc chiến chống lại chủng mới virus Corona (nCoV). Nhân viên cộng đồng tại các thành phố và làng mạc trên cả nước, đặc biệt ở những khu vực trong và xung quanh tâm dịch Vũ Hán, đang nỗ lực hết mình để bảo vệ hàng triệu người dân tại đây.\r\n\r\nQuân Giải phóng Nhân dân Trung Quốc đã gửi các đội quân y đến Vũ Hán để giúp xây dựng các bệnh viện và chuyển đổi các tòa nhà thành cơ sở y tế dã chiến. Các doanh nghiệp đã đẩy mạnh sản xuất nhu yếu phẩm như khẩu trang y tế. Sức mạnh tổng lực của các tầng lớp nhân dân Trung Quốc được thể hiện đầy đủ trong những thời điểm khó khăn này.\r\n\r\nTuy nhiên, tờ báo của Trung Quốc nhận định dù nCoV là một mối đe dọa nghiêm trọng, nhưng còn có một loại dịch hại khác có thể lây lan cùng với nó, mang tên “chủ nghĩa phân biệt chủng tộc và bài Trung Quốc”, thứ đang khiến nhiều người đổ lỗi cho người dân Trung Quốc về loại virus này, và khiến nhiều người Trung Quốc trở nên mặc cảm, thấy rằng bản thân mình không khác gì một loại virus.\r\n\r\nRất nhiều tin tức và bình luận tiêu cực về người Trung Quốc đã xuất hiện trong những ngày gần đây. Ví dụ, nhật báo Khao Sod của Thái Lan hôm 3.2 đăng tin một chủ nhà hàng ở tỉnh Chiang Mai đã treo biển cấm khách hàng Trung Quốc dùng bữa tại đây. Đến ngày hôm sau (4.2), nhật báo này đưa tin một du khách Trung Quốc và con của cô gần như phải ngủ trên vệ đường ở thị trấn nghỉ mát Hua Hin, do không có khách sạn nào muốn cấp phòng cho 2 người.', 1, '2020-02-06 18:11:35');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pass` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `website` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `yahoo` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `bio` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `user_level` tinyint(5) NULL DEFAULT NULL,
  `active` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `registration_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'lo', 'van phuong', 'lovanphuong09091996@gmail.com', '1234', 'qqq.com', 'qqq.yahoo', 'quan que', NULL, 2, NULL, '2020-02-04 13:36:48');

SET FOREIGN_KEY_CHECKS = 1;
