<?php $title = ''; include('includes/header.php');?>
<?php include('includes/mysqli_connect.php');?>
<?php include('includes/function.php');?>
<?php include('includes/sidebar-a.php'); ?>
<div id="content">
    <?php 
        if(isset($_GET['uid']) && filter_var($_GET['uid'],FILTER_VALIDATE_INT, array('min_range' => 1))) {
            $uid = $_GET['uid'];

            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                if(isset($_POST['delete']) && $_POST['delete'] == 'yes') {
                    // Neu muon xoa ....
                    // Yeu cau phai co ket noi csdl
                    $mysqli = new mysqli('localhost', 'root', '', 'izcms');

                    // Kiem tra xem ket noi co ton tai hay ko
                    check_db_conn();

                    $q = "DELETE FROM users WHERE user_id = ?";

                    if($stmt = $mysqli->prepare($q)) {

                        // Gan tham so cho prepare
                        $stmt->bind_param('i', $uid);

                        // Chay query
                        $stmt->execute() or die("MySQL Error: $q" . $stmt->error());

                        if($stmt->affected_rows == 1) {
                            $message = "<p class='success'>User was deleted successfully.</p>";
                        } else {
                            $message = "<p class='error'>User was NOT deleted due to a system error.</p>";
                        }
                        $stmt->close();
                    } // End if prepare
                } else {
                    $message = "<p class='warning'>I thought so too, shouldn't be deleted.</p>";
                }
            }// END if($_SERVER)

        } else {
            // Neu User ID khong ton tai tai dinh huong nguoi dung ve trang manage user
            redirect_to('admin/manage_users.php');
        }
    ?>